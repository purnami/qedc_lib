package com.example.mob_libsmartcard_qedc_lib;

import androidx.appcompat.app.AppCompatActivity;

import android.app.Activity;
import android.app.PendingIntent;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.nfc.NfcAdapter;
import android.os.Bundle;
import android.os.Handler;
import android.text.method.ScrollingMovementMethod;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.Iterator;

import id.co.softorb.lib.helper.Hex;
import id.co.softorb.lib.passti.ClientServerListener;
import id.co.softorb.lib.passti.STIUtility;
import id.co.softorb.lib.passti.emv;
import id.co.softorb.lib.passti.helper.BytesUtil;
import id.co.softorb.lib.smartcard.PASSTIListener;

import static id.co.softorb.lib.passti.helper.ErrorCode.ERR_UNKNOWN;
import static id.co.softorb.lib.passti.helper.ErrorCode.NOT_OK;

public class MainActivity extends Activity implements PASSTIListener {

    int code;
    TextView tvTeks;
    Button initpower, checkbalance, deduct, initdevice,initlib;
    EditText nominal,samtype,samslot;
    private static final String TAG = "MainActivity";
    String tagclass= this.getClass().getSimpleName();
    Context ctx;


    private ProgressDialog progressBar;
    STIUtility sti;
    int trxcounter;
    byte[] PIN = {(byte) 0x01, (byte) 0x23, (byte) 0x45, (byte) 0x67, (byte) 0x89, (byte) 0xAB, (byte) 0xCD, (byte) 0xEF};
    byte[] ins_id = {(byte) 0x00, (byte) 0x01};
    byte[] tid = {(byte) 0x12, (byte) 0x34, (byte) 0x56, (byte) 0x78};


    byte[] deviceTID={(byte) 0x22, (byte) 0x33, (byte) 0x44, (byte) 0x55};
    byte[] deviceMID={(byte) 0xAA, (byte) 0xBB, (byte) 0xCC, (byte) 0xDD,(byte) 0xEE, (byte) 0xFF, (byte) 0x44, (byte) 0x55};
    String cashlezz="DFE29C4496564B4A8E05A6CD9BD83A30";
    String passti_kit="758F40D46D95D1641448AA19B9282C05";
    String bnisam7050000000000210_mrgcode="9E351D7AB236572589FDB3378D568898";
    String bnisam1100000000599891_mrgcode="FC78C9C643302CC039807F6116DDDCCD";
    String dummy_BNI_MID="0000000000000000";
    String dummy_BNI_TID="12345678";
    String dummy_BRI_Procode="801020";
    String dummy_BRI_Batch="03";
    String dummy_BRI_MID="FFFFFFFFFFFFFFFF";
    String dummy_BRI_TID="FFFFFFFF";

    private int progressBarStatus = 0;
    private Handler progressBarbHandler = new Handler();
    String action="";

    int OK = 0;
    int device ;

    Intent intent;
    PendingIntent pendingIntent;

    int brirefno;
    ClientServerListener callback;

    Spinner spinner;

    int counter=0;


    int initialize_bankparam()
    {
        int code;
        int res;
        res=sti.initSAMVar_Mandiri(BytesUtil.bytes2HexString(PIN),BytesUtil.bytes2HexString(ins_id),BytesUtil.bytes2HexString(tid));//PIN,insID,TID
        if(res!=OK)
        {
            tvTeks.setText("initSAMVar_Mandiri, code "+res);
            return -2;
        }


        res=sti.initSAMVar_BNI(dummy_BNI_MID,dummy_BNI_TID,bnisam7050000000000210_mrgcode);//MID,TID,MarriedCode
//        res=sti.initSAMVar_BNI(dummy_BNI_MID,dummy_BNI_TID,bnisam1100000000599891_mrgcode);//MID,TID,MarriedCode

        if(res!=OK)
        {
            tvTeks.setText("initSAMVar_BNI, code "+res);
            return -4;
        }

        res=sti.initSAMVar_BRI(dummy_BRI_Procode,dummy_BRI_Batch,dummy_BRI_MID,dummy_BRI_TID);//procode,batchno,merchantID,TID
        if(res!=OK)
        {
            tvTeks.setText("initSAMVar_BRI, code "+res);
            return -3;
        }
        tvTeks.setText("Bank params init successful");
        return OK;
    }
    void InitScrollableTextView()
    {
        tvTeks = findViewById(R.id.tvTitle);
        tvTeks.setMovementMethod(new ScrollingMovementMethod());
    }

    @Override
    public void onNewIntent(Intent intent) {
        super.onNewIntent(intent);
        setIntent(intent);
        this.intent=intent;
        resolveIntent(intent);
    }

    private void resolveIntent(Intent intent) {
        this.intent=intent;
        Log.d("intentt", ""+intent);
        String action = intent.getAction();

    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        intent=new Intent(this, this.getClass());

        getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
        InitScrollableTextView();

        initlib = findViewById(R.id.bInitLib);
        initpower = findViewById(R.id.bInitPower);
//        clearscreen=findViewById(R.id.bClearScreen);

        checkbalance = findViewById(R.id.bCheckBalance);
        deduct = findViewById(R.id.bDeduct);
//        msr = findViewById(R.id.bMSR);
//        ctl = findViewById(R.id.bCTL);

        samtype = findViewById(R.id.etSAM);
        samslot = findViewById(R.id.etSAMSlot);
        nominal = findViewById(R.id.etPaymentDeduct);


//        findcontact = findViewById(R.d.bDetectContact);
//        print = findViewById(R.id.bPrint);i
        initdevice = findViewById(R.id.bInitDevice);
        trxcounter=0;
//        aposversion = findViewById(R.id.valueDeviceLibVersion);
//        passtiversion = findViewById(R.id.valuePasstiVersion);



//        devicetype=findViewById(R.id.valueDeviceType);
        //devinfo.others = new byte[2];//rfu
//        login = findViewById(R.id.bLogin);
//        post = findViewById(R.id.bPost);
        trxcounter=0;
        brirefno=0;
        //callback=this;
        ctx = getApplicationContext();
        String path=ctx.getApplicationInfo().nativeLibraryDir;
//        spinner = (Spinner) findViewById(R.id.DropDownDevice);
//        spinner.setOnItemSelectedListener(this);
        device=12;//set default to wizar
/***********************Important***********************/
//        findcontact.setOnClickListener(clickfindcontact);
//        print.setOnClickListener(clickprint);
//        clearscreen.setOnClickListener(clickclear);

        deduct.setOnClickListener(clickdeduct);

        checkbalance.setOnClickListener(clickcheckbalance);

        initdevice.setOnClickListener(clickinitdevice);

        initlib.setOnClickListener(clickinitlib);

        initpower.setOnClickListener(clickinitpower);

//        post.setOnClickListener(clickpostdata);

//        login.setOnClickListener(clicklogin);

//        msr.setOnClickListener(clickmsr);
//        ctl.setOnClickListener(clickfindctl);

    }

    View.OnClickListener clickinitdevice=new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            try {

                bindSdkDeviceService();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    };

    View.OnClickListener clickdeduct=new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            int res;
            if (nominal.getText().toString() == null || nominal.getText().toString().matches("")) {
                showText("Fill amount coloumn first!");
                nominal.setError("Can't be blank/empty");
                return;
            }

            initialize_bankparam();
            ShowProgressBar();
            new Thread(new Runnable() {
                @Override
                public void run() {
                    if(sti == null){
                        progressBar.dismiss();
                        showToast("Init Lib dahulu");
                        return;
                    }
                    if(!sti.checkInitLib()){
                        progressBar.dismiss();
                        showToast("Init Lib dahulu");
                        return;
                    }
                    int i = 0;

                    try{
                        trxcounter++;
                        brirefno++;
                        Log.d("trxcounterr", ""+trxcounter);
                        Log.d("brirefno", ""+brirefno);
                        sti.SetTrxCounter(trxcounter);
                        String strBRIRef = String.format("%06d",brirefno);
                        Log.d(TAG,"strBRIRef "+strBRIRef);
                        sti.SetBRIRefNo(strBRIRef);

                        String action = getIntent().getAction();
                        while (!NfcAdapter.ACTION_TAG_DISCOVERED.equals(action)){
                            action = getIntent().getAction();
                        }
                        sti.setIntent(getIntent());

                        i = sti.deduct(nominal.getText().toString());
                    }
                    catch(Exception e)
                    {
                        Log.e(TAG,"sti.deductt "+e.getMessage());
                        i= NOT_OK;
                    }
                    //if (i != OK && sti.getMandiriCorrection() || sti.getBRICorrection() || sti.getBNICorrection()) {
                    int code = sti.getActionCode();
                    //sti.CancelRepurchase();

                    Log.e(TAG,"sti.getAction,code "+code);
                    if (i != OK && code==1) {
                        String text="";


                        final String txt2display = text+"Deduct fail, Need Correction";
                        showText(txt2display);
                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                //tvTeks.setText(txt2display);
                                progressBar.dismiss();
                                Toast.makeText(MainActivity.this, txt2display, Toast.LENGTH_SHORT).show();
                            }
                        });

                    }else if (i != OK && i == -102) {
                        showText("Gagal Deduct - Saldo Kurang\nSaldo: " + sti.getBalance() + "\nPotongan Deduct: " + nominal.getText().toString());
                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {

                                progressBar.dismiss();
                                Toast.makeText(MainActivity.this, "Gagal Deduct", Toast.LENGTH_SHORT).show();
                            }
                        });
                    }
                    else if (i != OK) {
                        int finalI = i;
                        showText("Gagal Deduct\nResponseCode: " + finalI);
                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {

                                progressBar.dismiss();
                                Toast.makeText(MainActivity.this, "Gagal Deduct", Toast.LENGTH_SHORT).show();
                            }
                        });

                    } else {

                        final String pesan = "Saldo Awal: " + sti.getBalance() + "\nSaldo Akhir: " + sti.getLastBalance() + "\nCardNum: " + sti.getCardNo() + "\nPASSTILog: " + BytesUtil.bytes2HexString(sti.getPASSTILog());
                        showText(pesan);
                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                Log.d(TAG,"Messages: " + pesan);
                                //printing(sti.getBalance(),sti.getLastBalance(),sti.getCardNo());

                                progressBar.dismiss();
                                Toast.makeText(MainActivity.this, "Sukses Deduct", Toast.LENGTH_SHORT).show();
                            }
                        });
                    }
                }
            }).start();
        }};

    View.OnClickListener clickinitpower= new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            //
            //type*
            //Luminos: 1
            //Mandiri: 2
            //BRI: 3
            //BNI: 4
            //for APOS with only 1 slot, fill with: 1
            //
            if(sti == null){
                showToast("Init Device dahulu");
                return;
            }
            if(!sti.checkInitLib()){
                showToast("Init Library dahulu");
                return;
            }
            if (samtype.getText().toString() == null || samtype.getText().toString().matches("")) {
                samtype.setError("Can't be blank/empty");
                return;
            }
            if (samslot.getText().toString() == null || samslot.getText().toString().matches("")) {
                samslot.setError("Can't be blank/empty");
                return;
            }
            String message = "";

            int iRet;
            int samBankType = Integer.valueOf(samtype.getText().toString());
            int samSlot = Integer.valueOf(samslot.getText().toString());
            Log.d("samBankType",""+samBankType);
            Log.d("samSlot", ""+samSlot);

            iRet = sti.initCTL_SAM(samBankType,samSlot);
            switch (samBankType) {
                case 1:
                    message = "Luminos : ";
                    break;
                case 2:
                    message = "Mandiri : ";
                    break;
                case 3:
                    message = "BRI : ";
                    break;
                case 4:
                    message = "BNI : ";
                    break;
                default:
                    message = "Unknown Issuer : ";
                    break;
            }
            message+= GetCodeDesc(iRet);


            showText(message);
        }
    };
    View.OnClickListener clickinitlib= new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            try{


                if(sti == null){
                    showToast("Init Device dahulu");
                    return;
                }
                int res = sti.initLib(passti_kit);
                Log.d("res", ""+res);
                //int res = sti.initLib("4B2AA2F02AEF40C7A4F8EE425BFF4555");

                if(res==0) {
                    sti.initBank();

                    code=sti.SetMID(deviceMID);
                    if(code!=OK)
                    {
                        tvTeks.setText("SetMID fail, code "+code);
                        return;
                    }
                    code=sti.SetTID(deviceTID);
                    if(code!=OK)
                    {
                        tvTeks.setText("SetTID fail, code "+code);
                        return;
                    }

                }
                showText("Init Lib"  + "\nResponseCode: " + res);
            }catch (Exception e){
                e.printStackTrace();
            }
        }
    };


    public void ShowProgressBar() {
        Log.e(TAG, tagclass + "." + new Throwable()
                .getStackTrace()[0]
                .getMethodName());
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                progressBar = new ProgressDialog(MainActivity.this);
                progressBar.setCancelable(false);
                progressBar.setMessage("Process is running\nDO NOT REMOVE CARD ");
                progressBar.setProgressStyle(ProgressDialog.STYLE_SPINNER);
                progressBar.setProgress(0);
                progressBar.setMax(100);
                progressBar.show();
                progressBarStatus = 0;
            }
        });
    }


    View.OnClickListener clickcheckbalance=new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            initialize_bankparam();

            ShowProgressBar();

            new Thread(new Runnable() {
                @Override
                public void run() {
                    try {
                        if(sti == null){
                            progressBar.dismiss();
                            showToast("Init Lib dahulu");
                            return;
                        }
                        if(!sti.checkInitLib()){
                            progressBar.dismiss();
                            showToast("Init Lib dahulu");
                            return;
                        }
                        String action = getIntent().getAction();
                        while (!NfcAdapter.ACTION_TAG_DISCOVERED.equals(action)){
                            action = getIntent().getAction();
                        }
                        Log.d("getintentt", ""+getIntent());
                        sti.setIntent(getIntent());
                        int i = sti.checkbalance();

                        if (i != OK) {
                            showText("Gagal Check Balance\nResponse Code: " + i);
                            runOnUiThread(new Runnable() {
                                @Override
                                public void run() {

                                    if (progressBar != null)
                                        progressBar.dismiss();

                                    //Toast.makeText(TestActivity.this, "Gagal Check Balance", Toast.LENGTH_SHORT).show();
                                }
                            });

                        } else {
                            showText("Saldo: " + sti.getBalance() + "\nCardNo: " + sti.getCardNo());
                            runOnUiThread(new Runnable() {
                                @Override
                                public void run() {
                                    if (progressBar != null) progressBar.dismiss();

                                    // Toast.makeText(TestActivity.this, "Sukses Check Balance", Toast.LENGTH_SHORT).show();
                                }
                            });

                        }


                    } catch (Exception e) {
                        e.printStackTrace();
                        showText("Exception : "+e.getMessage());
                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                if (progressBar != null) progressBar.dismiss();

                                // Toast.makeText(TestActivity.this, "Exception : "+e.getMessage(), Toast.LENGTH_SHORT).show();
                            }
                        });
                    }
                }
            }).start();
        }
    };

    private void bindSdkDeviceService() {
        Log.d("device2", ""+device);
        String teks;
        int result=ERR_UNKNOWN;
        try{

            Log.d("stii",""+sti+" result"+result);

            if(sti==null)
            {

                sti = new STIUtility(getApplicationContext(),this);
                Log.d("stiii",""+sti);

                pendingIntent = PendingIntent.getActivity(getApplicationContext(), 0,
                        intent.addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP), 0);

                Log.d("getIntent", ""+getIntent());
                sti.setPendingIntent(pendingIntent, MainActivity.this);

                result=sti.SetDeviceService(this,device);//10=pax,9=wizar

                Log.d(" resultt", String.valueOf(result));

//                    passtiversion.setText(sti.getLibVersion());
//                    aposversion.setText(sti.DeviceLibVersion());
//                    devicetype.setText(Integer.toString(sti.DeviceType()));
            }

            //teks = "Serial #: " + posTerminal. + "\n"+ "OSVersion: " + deviceInfo.getAndroidOSVersion() + "\n" + "IMEI: " + deviceInfo.getIMEI() + "\n";
            // Log.d(TAG, teks);
            Log.d(" resulttt", String.valueOf(result));
            if(result!=OK)
            {
                teks="Device init. fail, code : "+GetCodeDesc(result);
            }
            else {
                teks = "Init device successful";
            }

        }catch (Exception e){
            e.printStackTrace();
            teks="Device init. fail, message "+e.getMessage();

        }
        showText(teks);
    }

    @Override
    protected void onDestroy() {
        Log.d(TAG,".onDestroy");
        super.onDestroy();
        if(sti!=null)
        {
            sti.CloseDevice();
        }

    }

    public void showToast(String msg) {
        showToastOnUI(msg);
    }

    private void showToastOnUI(final String msg) {
        runOnUiThread(new Runnable() {

            @Override
            public void run() {
                Toast.makeText(getApplicationContext(), msg, Toast.LENGTH_SHORT).show();
            }
        });

    }

    private void showText(final String text)
    {
        new Thread() {
            public void run() {

                try {
                    runOnUiThread(new Runnable() {

                        @Override
                        public void run() {
                            tvTeks.setText(text);
                            tvTeks.scrollTo(0, 0);
                            tvTeks.invalidate();
                        }
                    });
                    Thread.sleep(300);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }

        }.start();

    }

    String GetCodeDesc(int code) {
        switch (code) {
            default:
                return Integer.toString(code);
        }
    }

    @Override
    public void onRFDetected(byte[] uid, int mode, ArrayList[] appdata) {
        Log.e(TAG, tagclass + "." + new Throwable()
                .getStackTrace()[0]
                .getMethodName());

        if(uid==null)
        {
            showText("RF card not found");
            return;

        }
        showText("UID : "+ Hex.bytesToHexString(uid));
        showCardInformation( uid,  appdata);


    }

    private void showCardInformation(byte[] uid, ArrayList[]  appdata)
    {
        ArrayList<emv.tag> taglist;
        emv.tag currenttag;
        Iterator itr;
        String text="";
        int offset;
        text+="atr/uid : "+Hex.bytesToHexString(uid);
        if(appdata==null)
        {
            text+="\nNo application found";
        }
        else {
            text += "\ntotal apps : " + appdata.length + "\n";

            for (offset = 0; offset < appdata.length; offset++) {
                text += "=======================================================\n";
                text += "Application no : " + (offset + 1) + "\n";
                taglist = (ArrayList<emv.tag>) appdata[offset];

                itr = taglist.iterator();
                while (itr.hasNext()) {
                    currenttag = (emv.tag) itr.next();
                    if (currenttag.Value() != null) {
                        text += "tag : " + Hex.bytesToHexString(currenttag.Key()) + ", value : \n" + Hex.bytesToHexString(currenttag.Value()) + "\n";
                    }
                }
            }
        }
        showText(text);
    }
    @Override
    public void onContactDetected(byte[] uid, ArrayList[]  appdata) {
        Log.e(TAG, tagclass + "." + new Throwable()
                .getStackTrace()[0]
                .getMethodName());
        if(uid==null && appdata==null)
        {
            showText("No card detected.");
            return;
        }
        if(uid!=null && appdata==null)
        {
            showText("Incomplete process, please retry");
            return;
        }
        showCardInformation( uid,  appdata);
    }

    @Override
    public void onMSRDetected(byte[][] trackdata) {
        Log.e(TAG, tagclass + "." + new Throwable()
                .getStackTrace()[0]
                .getMethodName());
        int track=0;
        String text="";
        if(trackdata==null)
        {
            showText("no msr swiped");
            return;
        }
        for(track=0;track<trackdata.length;track++)
        {
            text+="track "+(track+1)+" : "+Hex.bytesToHexString(trackdata[track])+"\n";
        }
        showText(text);
    }

    @Override
    public void onSAMDetected(int slot, byte[] atr) {
        Log.e(TAG, tagclass + "." + new Throwable()
                .getStackTrace()[0]
                .getMethodName());
    }

    @Override
    public void onPinPadHandler(byte[] data) {
        Log.e(TAG, tagclass + "." + new Throwable()
                .getStackTrace()[0]
                .getMethodName());
    }
}